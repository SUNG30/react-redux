import React from 'react'
import CardComponent from '../../components/common/card/CardComponent'
import { useState, useEffect } from 'react'

export default function Home() {
    // Product state

    const [products , setProduct] = useState([])
    // fetch api 

    useEffect(()=>{
      fetch('https://api.escuelajs.co/api/v1/products')
      .then((response) => response.json())
      .then((data) => {
        setProduct(data)
      })
      .catch((error) => console.log(error));
    },[])
    console.log('Product:',products)
    //Product Card Rendering 
    function productItemRendering(item,index){
        return <CardComponent title={item.title} image={item.images}/>
    }
  return (
    <div className='flex flex-wrap mx-auto gap-7 mt-10 justify-center'>
        {products.map(productItemRendering)}
    </div>
  )
}
