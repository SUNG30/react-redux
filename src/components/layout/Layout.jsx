import React from 'react';
import { Outlet } from "react-router-dom";
import NavBarComponent from '../heading/NavBarComponent';

export default function Layout() {
  return (
    <>
        <header>
            <NavBarComponent/>
        </header>
        <Outlet/>
    </>
  )
}
