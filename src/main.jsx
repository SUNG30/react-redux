import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'

// Create react route in the main jsx

import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";

// Page
import Home from './page/Home/home.jsx';
import Layout from './components/layout/Layout.jsx';



// Create router for the page
const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    children: [
      {
        path: "/",
        element: <Home />,
      },
      // {
      //   path: "/about ",
      //   element: <About />,
      // },
      {

      }
    ],
  },
]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
      <RouterProvider router={router} />
  </React.StrictMode>,
)
